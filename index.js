const CorestoreNetworker = require('@corestore/networker')
const debug = require('@coboxcoop/logger')('@coboxcoop/networker')
const FeedNetworker = require('@coboxcoop/multifeed/networker')
const { MuxerTopic } = require('@coboxcoop/multifeed/networker')

class CoboxNetworker extends CorestoreNetworker {
  constructor (corestore, identity, lastSeenDb) {
    super(corestore, { keyPair: identity })
    this.identity = identity
    this.lastSeenDb = lastSeenDb
    this.multinet = new FeedNetworker(this)
    this.peersOnline = {}
    this.on('peer-add', this._onPeerAdd.bind(this))
    this.on('peer-remove', this._onPeerRemove.bind(this))
  }

  async _onPeerOnline (peer) {
    if (this.lastSeenDb.isClosed()) return debug('onPeerOnline: lastSeenDb is closed')
    const peerId = hex(peer.remotePublicKey)
    const payload = { peerId: peerId, lastSeenAt: Date.now(), online: true }
    try {
      await this.lastSeenDb.put(peerId, payload)
    } catch (err) {
      debug({ msg: 'failed to save to lastSeenDb', err })
    }
  }

  async _onPeerOffline (peer) {
    if (this.lastSeenDb.isClosed()) return debug('onPeerOffline: lastSeenDb is closed')
    const peerId = hex(peer.remotePublicKey)
    const payload = { peerId: peerId, lastSeenAt: Date.now(), online: false }
    try {
      await this.lastSeenDb.put(peerId, payload)
    } catch (err) {
      debug({ msg: 'failed to save to lastSeenDb', err })
    }
  }

  _onPeerAdd (peer) {
    const peerId = hex(peer.remotePublicKey)
    this.peersOnline[peerId] = (this.peersOnline[peerId] + 1) || 1
    if (this.peersOnline[peerId] === 1) {
      this._onPeerOnline(peer)
    }
  }

  _onPeerRemove (peer) {
    const peerId = hex(peer.remotePublicKey)
    this.peersOnline[peerId] = (this.peersOnline[peerId] - 1)
    if (this.peersOnline[peerId] < 1) {
      this._onPeerOffline(peer)
    }
  }

  muxer (address, getFeed) {
    return new MuxerTopic(this.corestore, address, { getFeed, timeout: false })
  }

  leave (address, opts) {
    this.configure(address, {
      ...opts,
      announce: false,
      lookup: false
    })
  }

  join (address, opts) {
    this.configure(address, {
      ...opts,
      announce: true,
      lookup: true
    })
  }
}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  return buf
}

module.exports = CoboxNetworker
