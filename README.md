# Networker

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

## Table of Contents

  - [About](#about)
  - [How does it work?](#how-does-it-work?)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About

**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`networker` contains a subclass of `CorestoreNetworker` to use with our fork of [`Multifeed`](https://gitlab.com/coboxcoop/multifeed). It contains any additional protocol extensions used (except multifeed's), and an overall description of the way the networking in the stack functions.

## How does it work?

CoBox uses [hyperswarm](https://github.com/hyperswarm/hyperswarm) to discover peers on a distributed hash table and exchange information about each-other, as well as replicate data. Data is replicated between peers using the Dat (now Hypercore) protocol. A single hypercore protocol stream is opened for each open socket with a peer. Multiple channels can be created for a given protocol stream instance, each of which corresponds to a discovery key. In CoBox we leverage the concept of a shared address and encryption key to define a distributed multiwriter file-system using multifeed. A discovery key is a hash of an address. This is handled internally, so all you need to provide is the address. Multifeed opens a channel on the existing protocol stream with a peer (or creates a new stream along with a channel for the provided address) and uses hypercore protocol extensions to implement a multiplexer which exchanges lists of hypercore public keys which are stored and fetched from the provided corestore instance.

## Install

```
npm i @coboxcoop/networker
```

## Usage

```js
const Corestore = require('corestore')
const CoBoxNetworker = require('@coboxcoop/networker')
const crypto = require('hypercore-crypto')

// generate some concept of a unique identity
// this will be used as the noise keypair
// names are subjective
const identity = Object.assign({ name: 'Grace' }, crypto.keyPair())

// define a corestore for storing hypercores
const corestore = Corestore(storage)

const network = CoBoxNetworker(corestore, identity)

// generate a multifeed address and join!
const address = crypto.randomBytes(32)
network.join(address)
```

## Credit

Thanks to @kira a.k.a. @noffle, @frando, @karissa, the magma-collective crew, the hyperdivision team and the cabal crew.

# License

[`AGPL-3.0-or-later`](./LICENSE)
